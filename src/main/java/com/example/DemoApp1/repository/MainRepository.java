package com.example.DemoApp1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.DemoApp1.model.Student;

@Repository
public interface MainRepository extends JpaRepository<Student, Integer> {

}
