package com.example.DemoApp1.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.example.DemoApp1.model.Student;

public class ExcelGenerator {

	public static ByteArrayInputStream studentsToExcel(List<Student> students) throws IOException {
		String[] COLUMNs = { "studentId", "studentName", "studentRoll" };
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Students");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Row for Header
			Row headerRow = sheet.createRow(0);

			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}

			// CellStyle for Age
			CellStyle ageCellStyle = workbook.createCellStyle();
			ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));

			int rowIdx = 1;
			for (Student student : students) {
				Row row = sheet.createRow(rowIdx++);

				row.createCell(0).setCellValue(student.getStudentId());
				row.createCell(1).setCellValue(student.getStudentName());
				// row.createCell(3).setCellValue(customer.getAge());
				Cell ageCell = row.createCell(3);
				ageCell.setCellValue(student.getStudentRoll());
				ageCell.setCellStyle(ageCellStyle);
			}

			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}

	public static List<Student> parseExcelFile(InputStream is) {
		try {
			Workbook workbook = new XSSFWorkbook(is);

			Sheet sheet = workbook.getSheet("Students");
			Iterator<Row> rows = sheet.iterator();

			List<Student> lstStudents = new ArrayList<Student>();

			int rowNumber = 0;
			while (rows.hasNext()) {
				Row currentRow = rows.next();

				// skip header
				if (rowNumber == 0) {
					rowNumber++;
					continue;
				}

				Iterator<Cell> cellsInRow = currentRow.iterator();

				Student cust = new Student();

				int cellIndex = 0;
				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();

					if (cellIndex == 0) { // ID
						cust.setStudentId((int) currentCell.getNumericCellValue());
					} else if (cellIndex == 1) { // Name
						cust.setStudentName(currentCell.getStringCellValue());
					} else if (cellIndex == 2) { // Age
						cust.setStudentRoll((int) currentCell.getNumericCellValue());
					}

					cellIndex++;
				}

				lstStudents.add(cust);
			}

			// Close WorkBook
			workbook.close();

			return lstStudents;
		} catch (IOException e) {
			throw new RuntimeException("FAIL! -> message = " + e.getMessage());
		}
	}

}