package com.example.DemoApp1.util;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.DemoApp1.model.Student;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class GeneratedPdfReport {
	public ByteArrayInputStream createPdf(List<Student> stu) throws DocumentException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		BaseColor darkGray = new BaseColor(30, 30, 30);
		Font f1 = new Font();
		f1.setColor(darkGray);

		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(117);
		table1.setWidths(new int[] { 20, 20, 20 });

		PdfPCell cell1 = new PdfPCell(new Phrase("id", f1));
		table1.addCell(cell1);

		PdfPCell cell2 = new PdfPCell(new Phrase("name", f1));
		table1.addCell(cell2);

		PdfPCell cell3 = new PdfPCell(new Phrase("roll", f1));
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table1.addCell(cell3);

		int size = stu.size();

		for (Student s : stu) {
			PdfPCell cell5;
			// cell5 = new PdfPCell(new Phrase(String.valueOf(s.getId()), f1));
			// cell5.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// table1.addCell(cell5);

			int id = s.getStudentId();
			String id1 = String.valueOf(id);
			cell5 = new PdfPCell(new Phrase(id1, f1));
			cell5.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table1.addCell(cell5);

			cell5 = new PdfPCell(new Phrase(s.getStudentName(), f1));
			cell5.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table1.addCell(cell5);

			int roll = s.getStudentRoll();
			String roll1 = String.valueOf(roll);
			cell5 = new PdfPCell(new Phrase(roll1, f1));
			cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(cell5);

			Rectangle layout = new Rectangle(360, 900);
			Document document = new Document(layout);
			PdfWriter.getInstance(document, out);
			document.open();
			document.add(table1);
			document.close();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

}
