package com.example.DemoApp1.util;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.DemoApp1.model.Parent;
import com.example.DemoApp1.model.Student;


@FeignClient(name="app2")
public interface StudentServiceProxy {
	@GetMapping(path="/app2",produces = "application/json")
	List<Parent> getDetails();
			
	

}
