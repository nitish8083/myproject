package com.example.DemoApp1.dao;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import com.example.DemoApp1.model.Student;
import com.example.DemoApp1.repository.MainRepository;
import com.example.DemoApp1.util.GeneratedPdfReport;
import com.itextpdf.text.DocumentException;

@Component
public class StudentDao {

	@Autowired
	MainRepository repo;

	@Autowired
	GeneratedPdfReport generatePdf;

	public void saveStudent(Student student) {
		repo.save(student);
	}

	public Optional<Student> find(int id) {
		return repo.findById(id);
	}

	public void delete(int id) {
		repo.deleteById(id);

	}

	public Student update(Student student) {
	
		return repo.save(student);
	}

	public InputStreamResource getStudentPdf() throws DocumentException {
		
		List<Student> stu = repo.findAll();
		ByteArrayInputStream bis = generatePdf.createPdf(stu);
		InputStreamResource input = new InputStreamResource(bis);

		return input;
	}

	public void upload(List<Student> students) {
		repo.saveAll(students);

	}

	public List<Student> getall() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
}
