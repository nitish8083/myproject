package com.example.DemoApp1.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import com.example.DemoApp1.dao.StudentDao;
import com.example.DemoApp1.model.Student;
import com.itextpdf.text.DocumentException;

@Service
public class MainService {

	@Autowired
	StudentDao dao;

	public void saveStudents(Student student) {
		// TODO Auto-generated method stub
		dao.saveStudent(student);
	}

	public Optional<Student> getServiceById(int id) {
		// TODO Auto-generated method stub
		return dao.find(id);
	}

	public void deleteById(int id) {
		// TODO Auto-generated method stub
		dao.delete(id);
	}

	public Student updatebyid(Student student) {

		return dao.update(student);
	}

	public InputStreamResource getStudentPdf() throws DocumentException {
		// TODO Auto-generated method stub
		return dao.getStudentPdf();
	}

	public void saveAll(List<Student> students) {
		// TODO Auto-generated method stub
		dao.upload(students);
	}

	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return dao.getall();
	}

}
