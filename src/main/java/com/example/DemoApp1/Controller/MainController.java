package com.example.DemoApp1.Controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.DemoApp1.model.Parent;
import com.example.DemoApp1.model.Student;
import com.example.DemoApp1.service.MainService;
import com.example.DemoApp1.util.ExcelGenerator;
import com.example.DemoApp1.util.StudentServiceProxy;
import com.itextpdf.text.DocumentException;

@RequestMapping("/app1")
@RestController
public class MainController {

	@Autowired
	StudentServiceProxy studentServiceProxy;

	@Autowired
	private MainService service;

	/**
	 * This Method create resource in h2 database
	 * @param student
	 * @return
	 */
	
	@PostMapping("/create")
	private ResponseEntity<String> create(@RequestBody Student student) {
		service.saveStudents(student);
		return new ResponseEntity<String>("saved", HttpStatus.OK);

	}

	/**
	 * Get Method to read resource
	 * 
	 * @return StudentModel and HttpStatus
	 */

	@GetMapping("/getbyid/{id}")
	public ResponseEntity<Optional<Student>> getServiceById(@PathVariable int id) {
		return new ResponseEntity<Optional<Student>>(service.getServiceById(id), HttpStatus.OK);
	}

	/**
	 * Delete Method to Delete resource
	 * 
	 * @return HttpStatus
	 */

	@DeleteMapping("delete-by-id/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") int id) {
		service.deleteById(id);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	/**
	 * Put Method to update StudentModel
	 * 
	 * @return StudentModel and HttpStatus
	 */

	@PutMapping("/update/{id}")
	public ResponseEntity<Student> update(@RequestBody Student student, @PathVariable("id") int id) {
		return new ResponseEntity<Student>(service.updatebyid(student), HttpStatus.OK);

	}

	/**
	 * this method fetch parent Details from Demoapp2
	 * 
	 * @return parent model
	 */

	@GetMapping(path = "/parent", produces = "application/json")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Parent> getDetails() {
		List<Parent> parent = studentServiceProxy.getDetails();
		return parent;

	}

	/**
	 * this method Generate pdf from database(H2)
	 * 
	 * @return pdf
	 * @throws DocumentException
	 */

	@GetMapping(path = "/getpdf/", produces = MediaType.APPLICATION_PDF_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)

	public InputStreamResource getStudentPdf() throws DocumentException {
		return service.getStudentPdf();

	}

	/**
	 * This method upload Excel file in database
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */

	@PostMapping("/upload")
	public ResponseEntity<String> save(@RequestParam("uploadfile") MultipartFile file) throws IOException {
		List<Student> students = ExcelGenerator.parseExcelFile(file.getInputStream());
		service.saveAll(students);
		return new ResponseEntity<String>("Uploaded successfully", HttpStatus.OK);
	}

	/**
	 * This method download excel file from database
	 * 
	 * @return
	 * @throws IOException
	 */

	@GetMapping(value = "/download/customers.xlsx")
	public ResponseEntity<InputStreamResource> excelCustomersReport() throws IOException {
		List<Student> students = (List<Student>) service.findAll();

		ByteArrayInputStream in = ExcelGenerator.studentsToExcel(students);
		// return IOUtils.toByteArray(in);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=students.xlsx");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

}
