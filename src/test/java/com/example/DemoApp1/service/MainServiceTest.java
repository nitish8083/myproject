package com.example.DemoApp1.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.DemoApp1.dao.StudentDao;
import com.example.DemoApp1.model.Student;
import com.example.DemoApp1.util.GeneratedPdfReport;

@ExtendWith(MockitoExtension.class)
class MainServiceTest {

	@InjectMocks
	private MainService services;

	@Mock
	private StudentDao dao;

	@Mock
	GeneratedPdfReport generatedPdfReport;

	@Test
	public void saveStudentTest() {
		Student student = new Student();
		student.setStudentId(1);
		student.setStudentName("Nitish");
		student.setStudentRoll(2);
		services.saveStudents(student);
		verify(dao, times(1)).saveStudent(student);
	}

	@Test
	public void getStudentByIdTest() {
		final Student student = new Student(1, "nitish", 12);
		Mockito.when(dao.find(1)).thenReturn(Optional.of(student));
		Optional<Student> expected = services.getServiceById(1);
		assertThat(expected).isNotNull();

	}

	@Test
	public void DeleteByIdTest() {
		Student student = new Student(1, "nitish", 12);

		services.deleteById(1);
		verify(dao, times(1)).delete(1);

	}

	@Test
	public void updateByIdTest() {
		Student student = new Student(1, "nitish", 12);
		Mockito.when(dao.update(student)).thenReturn(student);
		Student expected = services.updatebyid(student);
		assertThat(expected).isNotNull();
		verify(dao).update(student);

	}

}
