package com.example.DemoApp1.Controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.DemoApp1.model.Parent;
import com.example.DemoApp1.model.Student;
import com.example.DemoApp1.service.MainService;
import com.google.gson.Gson;

@ExtendWith(SpringExtension.class)
class MainControllerTest {

	@InjectMocks
	MainController mainController;

	@Mock
	MainService service;

	private MockMvc mockMvc;

	@BeforeEach
	public void setup() {

		mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();

	}

	
	@Test
	public void createStudentTest() throws Exception {
		Student student = new Student();
		String json = new Gson().toJson(student);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/app1/create");
		requestBuilder.contentType("application/json");
		requestBuilder.content(json);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Assertions.assertEquals("200", String.valueOf(result.getResponse().getStatus()));
		String content = result.getResponse().getContentAsString();
		assertEquals(content, "saved");
	}

	@Test
	public void getStudentTest() throws Exception {
		Student student = new Student();
		String json = new Gson().toJson(student);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/app1/getbyid/1");
		requestBuilder.contentType("application/json");
		requestBuilder.content(json);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Assertions.assertEquals("200", String.valueOf(result.getResponse().getStatus()));
	}

	@Test
	public void deleteStudent() throws Exception {
		String uri = "/app1/delete-by-id/2";
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(202, status);

	}

	@Test
	public void updateStudent() throws Exception {
		String uri = "/app1/update/1";
		Student student = new Student();
		student.setStudentName("nitish");

		String json = new Gson().toJson(student);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put(uri);
		requestBuilder.contentType("application/json");
		requestBuilder.content(json);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Assertions.assertEquals("200", String.valueOf(result.getResponse().getStatus()));
	}

	@Test
	public void getStudentPdfTest() throws Exception {
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/app1/getpdf/");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Assertions.assertEquals("201", String.valueOf(result.getResponse().getStatus()));
	}

}
