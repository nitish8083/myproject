package com.example.DemoApp1.dao;
import static org.assertj.core.api.Assertions.assertThat; 
import static org.mockito.Mockito.mockitoSession;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.DemoApp1.model.Student;
import com.example.DemoApp1.repository.MainRepository;
import com.example.DemoApp1.util.GeneratedPdfReport;
import com.itextpdf.text.DocumentException;

@org.junit.jupiter.api.extension.ExtendWith(SpringExtension.class)
class StudentDaoTest {

	@InjectMocks
	private StudentDao dao;
	
	@Mock
	private MainRepository repo;
	
	@Mock
	GeneratedPdfReport generatePdf;
	
	
	@Test
	void saveStudent() {
		Student student = new Student();
		student.setStudentId(1);
		student.setStudentName("Nitish");
		student.setStudentRoll(2);
		dao.saveStudent(student);
		 verify(repo, times(1)).save(student);
	}
	 @Test
	    public void getStudentByIdTest()
	    {
		final Student student = new  Student (1,"nitish",12);
		 Mockito.when(repo.findById(1)).thenReturn(Optional.of(student));
		Optional<Student> expected=dao.find(1);
		assertThat(expected).isNotNull();
		
	    }	
	 @Test
	    public void DeleteByIdTest()
	    {
		Student student = new  Student (1,"nitish",12);
		 
		dao.delete(1);
		verify(repo, times(1)).deleteById(1);
		
	    }	
	 @Test
	    public void updateByIdTest()
	    {
		Student student = new  Student (1,"nitish",12);
		Mockito.when(dao.update(student)).thenReturn(student);
		Student expected=dao.update(student);
		assertThat(expected).isNotNull();
		verify(repo).save(student);
	   
	    }
	 @Test
		public void getStudentPdfTest() throws DocumentException {
			List<Student> stu = new ArrayList<>();
			Student s = new Student();
			s.setStudentId(1);
			s.setStudentName("nitish");
			s.setStudentRoll(1);
			stu.add(s);
			Mockito.when(repo.findAll()).thenReturn(stu);
			byte[] buffer = new byte[10];
			ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
			Mockito.when(generatePdf.createPdf(Mockito.any())).thenReturn(bis);
			assertThat(dao.getStudentPdf()).isNotNull();
		}
	

	

}
